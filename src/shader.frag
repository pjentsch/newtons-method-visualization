#version 450
#define product(a, b) vec2(a.x*b.x-a.y*b.y, a.x*b.y+a.y*b.x)
#define conjugate(a) vec2(a.x,-a.y)
#define divide(a, b) vec2(((a.x*b.x+a.y*b.y)/(b.x*b.x+b.y*b.y)),((a.y*b.x-a.x*b.y)/(b.x*b.x+b.y*b.y)))
#define dist(a,b) (a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y)
#define axes_width 2.0
#define circle_radius 40.0
// layout(location=0) in vec3 a_position;
layout(location=0) out vec4 color;

layout(set=0, binding=0) 
uniform Uniforms {
    float win_width;
    float win_height;
    vec2 z[3];
    float scale; 
    uint max_iter;
    float eps;
    float offset_x;
    float offset_y;
};




vec3 hsv2rgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}
vec2 f(vec2 pt){
    vec2 z1n = pt - z[0];//should change these to loops when I confirm that z[] is used correctly
    vec2 z2n = pt - z[1];
    vec2 z3n = pt - z[2];
    return product(product(z1n,z2n),z3n);
}
vec2 f_prime(vec2 pt){
    vec2 z1n = pt - z[0];
    vec2 z2n = pt - z[1];
    vec2 z3n = pt - z[2];
    return product(z1n,z2n) + product(z1n,z3n) +  product(z2n,z3n);
}
void main(){

    // Shift the fragment coordinets based on the offset.
    vec2 shifted_coords =vec2(gl_FragCoord.x + offset_x/scale, gl_FragCoord.y + offset_y/scale) ;

    // // first make pixle coodinates between 0 and 1. Then scale to all 4 quadrents,
    // // then do final shift.
    vec2 pt = vec2(shifted_coords/ float(win_height))* (scale*2) - scale;

    if (abs(pt.x)/scale <=axes_width/(win_height) || abs(pt.y)/scale <= axes_width/win_height){
         color = vec4(0.5,0.5,0.5,1.0);
    }
    else if (distance(pt,z[0])/scale <= circle_radius/win_height){//should change these to loops when I confirm that z[] is used correctly
        color = vec4(0.5,0.5,0.5,1.0);  
    }
    else if (distance(pt,z[1])/scale <= circle_radius/win_height){
        color = vec4(0.5,0.5,0.5,1.0);  
    }
    else if (distance(pt,z[2])/scale <=  circle_radius/win_height){
        color = vec4(0.5,0.5,0.5,1.0);  
    }
    else{
        float i;
        for(i = 0; i < max_iter; i++) {
            pt = pt - divide(f(pt),f_prime(ptz));
            if (dist(pt,z[0]) < eps)//should change these to loops when I confirm that z[] is used correctly
            {
                color = vec4(hsv2rgb(vec3(0.0, 0.85,(max_iter - i)/(max_iter))),1.0);
                break;
            }
            else if (dist(pt,z[1]) < eps)
            {
                color = vec4(hsv2rgb(vec3(0.5, 0.85,(max_iter - i)/(max_iter))), 1.0);
                break;
            }
            else if (dist(pt,z[2]) < eps)
            {
                color = vec4(hsv2rgb(vec3(0.25, 0.85,(max_iter - i)/(max_iter))),0.0);
                break;
            }
        }
    }

}
use std::iter;
use std::convert::TryInto;
use wgpu::util::DeviceExt;
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
    window::{Window, WindowBuilder},
};

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable)]
struct Vertex {
    position: [f32; 3]
}

impl Vertex {
    fn desc<'a>() -> wgpu::VertexBufferDescriptor<'a> {
        use std::mem;
        wgpu::VertexBufferDescriptor {
            stride: mem::size_of::<Vertex>() as wgpu::BufferAddress,
            step_mode: wgpu::InputStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttributeDescriptor {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float3,
                }
            ],
        }
    }
}
const scale_interval: f32 = 1.05;
const VERTICES: &[Vertex] = &[
    Vertex {
        position: [1.0, 1.0, 0.0],
    }, // A
    Vertex {
        position: [-1.0, 1.0, 0.0],
    }, // B
    Vertex {
        position: [-1.0, -1.0, 0.0],
    }, // C
    Vertex {
        position: [1.0, -1.0, 0.0],
    }, // D
];
const INDICES: &[u16] = &[0, 1, 2,0,2,3];

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct Uniforms {
    win_width: f32,
    win_height: f32,
    p_zeros: [[f32;2];3],
    scale: f32,
    max_iter: u32,
    eps: f32,
    offset_x: f32,
    offset_y: f32,
}
fn dist(x_1: f32,y_1: f32, x_2:f32, y_2:f32 ) -> f32{
    return ((x_1 - x_2)*(x_1-x_2) + (y_1 - y_2)*(y_1-y_2)).sqrt();
}
impl Uniforms {
    fn new(win_width:f32, win_height:f32) -> Self {
        Self {
            win_width,
            win_height,
            p_zeros: [
                [1.0,0.0],
                [-0.5,-0.866],
                [-0.5,0.866],
            ],
            scale: 1.0,
            max_iter: 30,
            eps: 0.1,
            offset_x: 0.0,
            offset_y: 0.0,
        }
    }
}

struct State {
    surface: wgpu::Surface,
    device: wgpu::Device,
    queue: wgpu::Queue,
    sc_desc: wgpu::SwapChainDescriptor,
    swap_chain: wgpu::SwapChain,
    render_pipeline: wgpu::RenderPipeline,
    size: winit::dpi::PhysicalSize<u32>,
    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    num_indices: u32,
    uniforms: Uniforms,
    uniform_buffer: wgpu::Buffer,
    uniform_bind_group: wgpu::BindGroup,
    mouse_state: [ElementState ;2],
    clicked_root_circle: i32,
    mouse_pos: [f32; 2],
    mouse_pos_prev: [f32; 2],
    scaled_mouse_pos: [f32;2],
    real_view: bool,
}

impl State {
    async fn new(window: &Window) -> Self {
        let size = window.inner_size();

        // The instance is a handle to our GPU
        // BackendBit::PRIMARY => Vulkan + Metal + DX12 + Browser WebGPU
        let instance = wgpu::Instance::new(wgpu::BackendBit::PRIMARY);
        let surface = unsafe { instance.create_surface(window) };
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::Default,
                compatible_surface: Some(&surface),
            })
            .await
            .unwrap();
        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    features: wgpu::Features::empty(),
                    limits: wgpu::Limits::default(),
                    shader_validation: true,
                },
                None, // Trace path
            )
            .await
            .unwrap();

        let sc_desc = wgpu::SwapChainDescriptor {
            usage: wgpu::TextureUsage::OUTPUT_ATTACHMENT,
            format: wgpu::TextureFormat::Bgra8UnormSrgb,
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::Fifo,
        };
        let swap_chain = device.create_swap_chain(&surface, &sc_desc);

        let mut uniforms = Uniforms::new(size.width as f32,size.height as f32);
        let uniform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Uniform Buffer"),
            contents: bytemuck::cast_slice(&[uniforms]),
            usage: wgpu::BufferUsage::UNIFORM | wgpu::BufferUsage::COPY_DST,
        });

        let uniform_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStage::FRAGMENT,
                    ty: wgpu::BindingType::UniformBuffer {
                        dynamic: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
                label: Some("uniform_bind_group_layout"),
            });

        let uniform_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &uniform_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: wgpu::BindingResource::Buffer(uniform_buffer.slice(..)),
            }],
            label: Some("uniform_bind_group"),
        });

        let vs_module = device.create_shader_module(wgpu::include_spirv!("shader.vert.spv"));
        let fs_module = device.create_shader_module(wgpu::include_spirv!("shader.frag.spv"));

        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("Render Pipeline Layout"),
                bind_group_layouts: &[&uniform_bind_group_layout],
                push_constant_ranges: &[],
            });

        let render_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex_stage: wgpu::ProgrammableStageDescriptor {
                module: &vs_module,
                entry_point: "main",
            },
            fragment_stage: Some(wgpu::ProgrammableStageDescriptor {
                module: &fs_module,
                entry_point: "main",
            }),
            rasterization_state: Some(wgpu::RasterizationStateDescriptor {
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: wgpu::CullMode::Back,
                depth_bias: 0,
                depth_bias_slope_scale: 0.0,
                depth_bias_clamp: 0.0,
                clamp_depth: false,
            }),
            primitive_topology: wgpu::PrimitiveTopology::TriangleList,
            color_states: &[wgpu::ColorStateDescriptor {
                format: sc_desc.format,
                color_blend: wgpu::BlendDescriptor::REPLACE,
                alpha_blend: wgpu::BlendDescriptor::REPLACE,
                write_mask: wgpu::ColorWrite::ALL,
            }],
            depth_stencil_state: None,
            vertex_state: wgpu::VertexStateDescriptor {
                index_format: wgpu::IndexFormat::Uint16,
                vertex_buffers: &[Vertex::desc()],
            },
            sample_count: 1,
            sample_mask: !0,
            alpha_to_coverage_enabled: false,
        });

        let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Vertex Buffer"),
            contents: bytemuck::cast_slice(VERTICES),
            usage: wgpu::BufferUsage::VERTEX,
        });
        let index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Index Buffer"),
            contents: bytemuck::cast_slice(INDICES),
            usage: wgpu::BufferUsage::INDEX,
        });
        let num_indices = INDICES.len() as u32;

        Self {
            surface,
            device,
            queue,
            sc_desc,
            swap_chain,
            render_pipeline,
            vertex_buffer,
            index_buffer,
            num_indices,
            uniform_buffer,
            uniform_bind_group,
            uniforms,
            size,
            mouse_state: [ElementState::Released, ElementState::Released],
            clicked_root_circle: -1,
            mouse_pos: [0.0,0.0],
            mouse_pos_prev: [-1.0,-1.0],
            scaled_mouse_pos: [0.0,0.0],
            real_view: true,
        }
    }

    fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        self.size = new_size;
        self.sc_desc.width = new_size.width;
        self.sc_desc.height = new_size.height;
        self.uniforms.win_height = new_size.height as f32;
        self.uniforms.win_width = new_size.width as f32;
        println!("{}",self.uniforms.win_height);
        self.swap_chain = self.device.create_swap_chain(&self.surface, &self.sc_desc);
    }
    fn input(&mut self, event: &WindowEvent) -> bool {
        match event {
            WindowEvent::KeyboardInput{input,..} => {
                match input{
                    KeyboardInput{state,  virtual_keycode, ..} => 
                    match virtual_keycode {
                        Some(VirtualKeyCode::Up) => {
                            println!("{}",self.uniforms.max_iter);
                            self.uniforms.max_iter+=1;
                            return true;
                        },
                        Some(VirtualKeyCode::Down) => {
                            println!("{}",self.uniforms.max_iter);
                            self.uniforms.max_iter-=1;
                            return true;
                        },
                        Some(VirtualKeyCode::Left) => {
                            self.uniforms.eps-=0.01;
                            return true;
                        },
                        Some(VirtualKeyCode::Right) => {
                            self.uniforms.eps+=0.01;
                            return true;
                        },
                        _ => return false
                    }
                }
            },
            WindowEvent::MouseInput{device_id,state, button,..} => {
                match button{
                    MouseButton::Left => {
                        self.mouse_state[0] = *state;
                        return false;
                    },
                    MouseButton::Right => {
                        self.mouse_state[1] = *state;
                        match self.mouse_state[1]{
                            ElementState::Pressed => {
                                for (i,root) in self.uniforms.p_zeros.iter().enumerate(){
                                    if dist(self.scaled_mouse_pos[0],self.scaled_mouse_pos[1],root[0],root[1])/self.uniforms.scale < (self.uniforms.win_height as f32){
                                        println!("GOTTEM");
                                        self.clicked_root_circle = i.try_into().unwrap();
                                    }
                                }
                            }
                            ElementState::Released => {
                                self.clicked_root_circle = -1;
                            }
                        }
                        return false;
                    }
                    _ => false,
                }
            },
            WindowEvent::MouseWheel{
                device_id,
                delta: MouseScrollDelta::LineDelta(_, h),..} => {
                        if h > &0.0 {
                            self.uniforms.scale /= scale_interval;
                            println!("scale decremented to {}", self.uniforms.scale);
                        }
                        else if h < &0.0 {
                            self.uniforms.scale *= scale_interval;
                            println!("scale incremented to {}", self.uniforms.scale);
                        }
                        return true;
            },
            WindowEvent::CursorMoved{
                device_id,
                position,..} => {
                        self.mouse_pos[0] = position.x as f32;
                        self.mouse_pos[1] = position.y as f32;
                        let win_size_factor: f32 = self.uniforms.win_height;
                        let scale: f32 = self.uniforms.scale;
                        self.scaled_mouse_pos = [(self.mouse_pos[0]   + self.uniforms.offset_x/scale)/(win_size_factor as f32) * (scale*2.0)  - scale,
                        (self.uniforms.win_height - self.mouse_pos[0] + self.uniforms.offset_y/scale)/win_size_factor * (scale*2.0)  - scale] ;

                        match self.mouse_state[0] {
                            ElementState::Pressed => {
                                if self.mouse_pos_prev[0] >= 0.0 {
                                    let diff_x = self.mouse_pos[0] - self.mouse_pos_prev[0];
                                    let diff_y = self.mouse_pos[1] - self.mouse_pos_prev[1];
    
                                    self.uniforms.offset_x -= (diff_x as f32) * self.uniforms.scale;
                                    self.uniforms.offset_y -= (diff_y as f32) * self.uniforms.scale;
    
                                    println!("offset shifted ({},{})",self.uniforms.offset_x,self.uniforms.offset_y);
                                    // println!("({},{})",self.mouse_pos_prev[0],self.mouse_pos_prev[1]);
                                }
    
                                self.mouse_pos_prev = self.mouse_pos;
                            },
                            ElementState::Released => {
                                self.mouse_pos_prev = [-1.0,-1.0];
                            }
                        }
                        if !self.real_view{
                            match self.mouse_state[1] {
                                ElementState::Pressed => {
                                    self.uniforms.p_zeros[self.clicked_root_circle as usize][0] = self.scaled_mouse_pos[0];
                                    self.uniforms.p_zeros[self.clicked_root_circle as usize][1] = self.scaled_mouse_pos[1];
                                    for i in 0..self.uniforms.p_zeros.len(){
                                        if ((self.uniforms.p_zeros[i][1]).abs()<0.1){
                                            self.uniforms.p_zeros[i][1] = 0.0;
                                        }
                                    }
                                },
                                _ =>()
                            }
                        }
                        
                        return true;

            },
            _ => false,
        }
    }

    fn update(&mut self) {
        self.queue.write_buffer(
            &self.uniform_buffer,
            0,
            bytemuck::cast_slice(&[self.uniforms]),
        );
    }

    fn render(&mut self) -> Result<(), wgpu::SwapChainError> {
        let frame = self.swap_chain.get_current_frame()?.output;

        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        {
            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                color_attachments: &[wgpu::RenderPassColorAttachmentDescriptor {
                    attachment: &frame.view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(wgpu::Color {
                            r: 0.1,
                            g: 0.2,
                            b: 0.3,
                            a: 1.0,
                        }),
                        store: true,
                    },
                }],
                depth_stencil_attachment: None,
            });

            render_pass.set_pipeline(&self.render_pipeline);
            render_pass.set_bind_group(0, &self.uniform_bind_group, &[]);
            render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
            render_pass.set_index_buffer(self.index_buffer.slice(..));
            render_pass.draw_indexed(0..self.num_indices, 0, 0..1);
        }

        self.queue.submit(iter::once(encoder.finish()));

        Ok(())
    }
}

fn main() {
    env_logger::init();
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();

    use futures::executor::block_on;

    // Since main can't be async, we're going to need to block
    let mut state = block_on(State::new(&window));

    event_loop.run(move |event, _, control_flow| {
        match event {
            Event::WindowEvent {
                ref event,
                window_id,
            } if window_id == window.id() => {
                if !state.input(event) {
                    match event {
                        WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                        WindowEvent::Resized(physical_size) => {
                            println!("resize!");
                            state.resize(*physical_size);
                        },
                        WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                            // new_inner_size is &mut so w have to dereference it twice
                            state.resize(**new_inner_size);
                        }
                        _ => {}
                    }
                }
            }
            Event::RedrawRequested(_) => {
                state.update();
                match state.render() {
                    Ok(_) => {}
                    // Recreate the swap_chain if lost
                    Err(wgpu::SwapChainError::Lost) => state.resize(state.size),
                    // The system is out of memory, we should probably quit
                    Err(wgpu::SwapChainError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                    // All other errors (Outdated, Timeout) should be resolved by the next frame
                    Err(e) => eprintln!("{:?}", e),
                }
            }
            Event::MainEventsCleared => {
                // RedrawRequested will only trigger once, unless we manually
                // request it.
                window.request_redraw();
            }
            _ => {}
        }
    });
}
